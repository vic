#include "hex_map_rules.h"

const HexMapRules::Direction HexMapRules::NorthEast (NE);
const HexMapRules::Direction HexMapRules::East (E);
const HexMapRules::Direction HexMapRules::SouthEast (SE);
const HexMapRules::Direction HexMapRules::SouthWest (SW);
const HexMapRules::Direction HexMapRules::West (W);
const HexMapRules::Direction HexMapRules::NorthWest (NW);

HexMapRules::Direction::Direction (DirectionId dir) :
    dir (dir)
{
}

HexMapRules::Direction::~Direction ()
{
}

bool HexMapRules::Direction::checkBounds (unsigned int x, unsigned int y,
        unsigned int w, unsigned int h) const
{
    switch (dir) {
        case NE:
            return x<(w-1);
        case E:
            return x<(w-1) && y<(h-1);
        case SE:
            return y<(h-1);
        case SW:
            return x>0;
        case W:
            return x>0 && y>0;
        case NW:
            return y>0;
    }
    return false;
}

std::pair<unsigned int, unsigned int> HexMapRules::Direction::move (
        unsigned int x, unsigned int y) const
{
    switch (dir) {
        case NE:
            ++x;
            break;
        case E:
            ++x;
            ++y;
            break;
        case SE:
            ++y;
            break;
        case SW:
            --x;
            break;
        case W:
            --x;
            --y;
            break;
        case NW:
            --y;
            break;
    }
    return std::pair<unsigned int, unsigned int>(x, y);
}
