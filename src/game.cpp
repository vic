#include "game.h"

Game::Game (Board<Tile>* board) :
    board (board),
    units (new std::map<unsigned int, Unit*>()),
    turn (0)
{
}

Game::~Game ()
{
    delete(board);
    delete(units);
}

Board<Tile>* Game::getBoard ()
{
    return board;
}

std::map<unsigned int, Unit*>* Game::getUnitMap ()
{
    return units;
}

unsigned int Game::getTurn () const
{
    return turn;
}

void Game::setTurn (unsigned int turn)
{
    this->turn = turn;
}
