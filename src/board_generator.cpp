#include "board_generator.h"
#include <cstdlib>

std::map<std::string, IBoardGenerator*> IBoardGenerator::directory;

IBoardGenerator::IBoardGenerator (const std::string& id) :
    id (std::string(id))
{
}

IBoardGenerator::~IBoardGenerator ()
{
}

const std::string IBoardGenerator::getId () const
{
    return id;
}

int IBoardGenerator::subscribe (IBoardGenerator* generator)
{
    return 0;
}

IBoardGenerator* IBoardGenerator::get (const std::string& id)
{
    if (directory.find(id) != directory.end()) {
        return directory.find(id)->second;
    } else {
        return NULL;
    }
}
