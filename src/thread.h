#ifndef THREAD_H
#define THREAD_H

#include <pthread.h>

class IThread
{
    protected:
        pthread_t* thread;

    public:
        IThread () : thread (new pthread_t) { }
        virtual ~IThread () { delete(thread); }

        virtual void run () = 0;

        int start (pthread_attr_t* attr = NULL)
        {
            return pthread_create(thread, attr, Thread::entryPoint,
                    (void*) this);
        }

        int join (void **retval = NULL)
        {
            return pthread_join(*thread, retval);
        }

        const pthread_t& getThread () { return *thread; }

        static void* entryPoint (void* thread)
        {
            ((Thread*) thread)->run();
            return NULL;
        }
};

#endif /* THREAD_H */

