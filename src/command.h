#ifndef COMMAND_H
#define COMMAND_H

#include <cstdlib>
#include "game.h"

class ICommand
{
    public:
        ICommand () {};
        virtual ~ICommand () {};

        virtual int execute (Game* g) = 0;
        virtual int revert (Game* g) = 0;
};

#endif /* COMMAND_H */
