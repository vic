#ifndef MAP_H
#define MAP_H

#include <cstdlib>
#include <cstring>

template<class T>
class Board
{
    protected:
        unsigned int w;
        unsigned int h;
        T** tiles;

    public:
        Board (unsigned int w);
        Board (unsigned int w, unsigned int h);
        virtual ~Board ();

        bool insideBoard (unsigned int x, unsigned int y) const;

        const T* get (unsigned int x, unsigned int y) const;
        T* get (unsigned int x, unsigned int y);

        bool set (unsigned int x, unsigned int y, T* value);

    protected:
        unsigned int arrayIndex (unsigned int x, unsigned int y) const;
};

template<class T>
Board<T>::Board (unsigned int w) :
    w (w),
    h (w),
    tiles (new T*[w*h])
{
    memset(tiles, 0, sizeof(T*)*w*h);
}

template<class T>
Board<T>::Board (unsigned int w, unsigned int h) :
    w (w),
    h (h),
    tiles (new T*[w*h])
{
    memset(tiles, 0, sizeof(T*)*w*h);
}

template<class T>
Board<T>::~Board ()
{
    unsigned int max = w*h;
    for (unsigned int i=0; i<max; ++i) {
        if (tiles[i] != NULL) { delete(tiles[i]); }
    }
    delete[](tiles);
}

template<class T>
const T* Board<T>::get (unsigned int x, unsigned int y) const
{
    return (insideBoard(x, y)) ? tiles[arrayIndex(x, y)] : NULL;
}

template<class T>
T* Board<T>::get (unsigned int x, unsigned int y)
{
    return (insideBoard(x, y)) ? tiles[arrayIndex(x, y)] : NULL;
}

template<class T>
bool Board<T>::set (unsigned int x, unsigned int y, T* value)
{
    if (insideBoard(x, y)) {
        tiles[arrayIndex(x, y)] = value;
        return true;
    }
    return false;
}

template<class T>
bool Board<T>::insideBoard (unsigned int x, unsigned int y) const
{
    return x<w && y<h;
}

template<class T>
unsigned int Board<T>::arrayIndex (unsigned int x, unsigned int y) const
{
    return x*w+y;
}

#endif /* MAP_H */
