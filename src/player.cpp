#include "player.h"
#include <cstdlib>

Player::Player (unsigned int id, ICommandSource* cmdSrc) :
    id (id),
    cmdSrc (cmdSrc)
{
}

Player::~Player ()
{
    delete(cmdSrc);
}

unsigned int Player::getId () const
{
    return id;
}

ICommand* Player::nextCommand ()
{
    return cmdSrc->nextCommand();
}
