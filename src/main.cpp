#include <iostream>
#include <string>

#include "board.h"
#include "tile.h"
#include "direction.h"
#include "hex_map_rules.h"
#include "unit.h"
#include "command.h"
#include "game_container.h"

class DummyCommandSource :
    public ICommandSource
{
    public:
        DummyCommandSource () : ICommandSource () {}
        ~DummyCommandSource () {}

        ICommand* nextCommand ()
        {
            std::cout << "Press key" << std::endl;
            std::cin.get();
            return NULL;
        }
        bool hasNextCommand () { return false; }
};

class DummyGameRules :
    public IGameRules
{
    public:
        DummyGameRules () : IGameRules () {}
        ~DummyGameRules () {}

        bool gameEnded (const Game& g)
        {
            return g.getTurn() >= 3;
        }
};

void lala (const IDirection* dir);
int test (int argc, char** argv);

int main (int argc, char** argv)
{
    Game game(new Board<Tile>(10));

    IGameRules* rules = new DummyGameRules();

    GameContainer container(&game, rules);

    container.addPlayer(new Player(0, new DummyCommandSource()));

    container.start();

    delete(rules);

    return 0;
}

int test (int argc, char** argv)
{
    Board<Tile> board(3);

    board.set(0, 2, new Tile(0, 2));
    board.get(0, 2)->setProperty(std::string("key"), std::string("val"));

    std::cout << *board.get(0, 2)->getProperty(std::string("key")) << std::endl;

    if (board.get(0, 2) != NULL) {
        std::cout << "(0,2) does exist" << std::endl;
    }

    if (board.get(0, 0) == NULL) {
        std::cout << "(0,0) does not exist" << std::endl;
    }

    Unit u(3);
    u.setProperty(std::string("type"), std::string("archer"));

    std::cout << *u.getProperty(std::string("type")) << std::endl;

    std::cout << HexMapRules::West.checkBounds(0, 0, 1, 1) << std::endl;
    lala(&HexMapRules::East);

    return 0;
}

void lala (const IDirection* dir)
{
    std::cout << dir->checkBounds(0, 0, 2, 2) << std::endl;
}
