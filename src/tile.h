#ifndef TILE_H
#define TILE_H

#include "has_properties.h"

class Tile :
    public HasProperties
{
    protected:
        unsigned int x;
        unsigned int y;

    public:
        Tile (unsigned int x, unsigned int y);
        virtual ~Tile ();

        unsigned int getX () const;
        unsigned int getY () const;
};

#endif /* TILE_H */
