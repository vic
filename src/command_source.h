#ifndef COMMAND_SOURCE_H
#define COMMAND_SOURCE_H

#include "command.h"

class ICommandSource
{
    public:
        ICommandSource () {};
        virtual ~ICommandSource () {};

        virtual ICommand* nextCommand () = 0;
        virtual bool hasNextCommand () = 0;
};

#endif /* COMMAND_SOURCE_H */
