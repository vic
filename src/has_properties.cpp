#include "has_properties.h"

HasProperties::HasProperties () :
    properties (new std::map<std::string, std::string>())
{
}

HasProperties::~HasProperties ()
{
    delete(properties);
}

const std::string* HasProperties::getProperty (const std::string& key) const
{
    if (properties->find(key) != properties->end()) {
        return &properties->find(key)->second;
    } else {
        return NULL;
    }
}

bool HasProperties::setProperty (const std::string& key, const std::string& value)
{
    std::pair<std::string, std::string> newPair(key, value);
    std::pair<std::map<std::string, std::string>::iterator, bool> insertRet =
        properties->insert(newPair);
    return insertRet.second;
}

void HasProperties::eraseProperty (const std::string& key)
{
}
