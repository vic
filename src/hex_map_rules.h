#ifndef HEX_MAP_RULES_H
#define HEX_MAP_RULES_H

#include "direction.h"

class HexMapRules
{
    protected:
        enum DirectionId { NE, E, SE, SW, W, NW };

        class Direction :
            public IDirection
        {
            protected:
                DirectionId dir;

            public:
                Direction (DirectionId dir);
                ~Direction ();

                bool checkBounds (unsigned int x, unsigned int y,
                        unsigned int w, unsigned int h) const;

                std::pair<unsigned int , unsigned int> move (
                        unsigned int x, unsigned int y) const;
        };

    public:
        static const Direction NorthEast;
        static const Direction East;
        static const Direction SouthEast;
        static const Direction SouthWest;
        static const Direction West;
        static const Direction NorthWest;
};

#endif /* HEX_MAP_RULES_H */
