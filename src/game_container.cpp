#include "game_container.h"
#include <cstdlib>

GameContainer::GameContainer (Game* game, IGameRules* rules) :
    game (game),
    players(new std::list<Player*>),
    rules (rules),
    started (false),
    ended (false)
{
}

GameContainer::~GameContainer ()
{
    for (std::list<Player*>::iterator iter = players->begin();
            iter != players->end(); ++iter) { delete(*iter); }
    delete(players);
}

bool GameContainer::gameEnded () const
{
    return ended;
}

int GameContainer::addPlayer (Player* player)
{
    if (!started && player != NULL) {
        players->push_back(player);
        return 0;
    }
    return -1;
}

void GameContainer::start ()
{
    int END_GAME = 1;

    int result = 0;
    std::list<Player*>::iterator iter;

    started = true;
    while (result != END_GAME && players->size() > 0) {
        if (rules->gameEnded(*game)) {
            result = END_GAME;
        }

        for (iter = players->begin();
                iter != players->end() && result != END_GAME; ++iter) {
            result = onTurnBegin(*iter);
            result = doPlayerTurn(*iter);
            result = onTurnEnd(*iter);

            game->setTurn(game->getTurn()+1);
        }
    }
    ended = true;
}

int GameContainer::doPlayerTurn (Player* player)
{
    ICommand* cmd;

    while ((cmd = player->nextCommand()) != NULL) {
        if (cmd->execute(game) != 0) {
            cmd->revert(game);
        }
    }
    return 0;
}

int GameContainer::onTurnBegin (Player* player)
{
    return 0;
}

int GameContainer::onTurnEnd (Player* player)
{
    return 0;
}
