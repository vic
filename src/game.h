#ifndef GAME_H
#define GAME_H

#include "board.h"
#include "tile.h"
#include "unit.h"
#include <map>

class Game
{
    protected:
        Board<Tile>* board;
        std::map<unsigned int, Unit*>* units;
        unsigned int turn;

    public:
        Game (Board<Tile>* board);
        virtual ~Game ();

        Board<Tile>* getBoard ();
        std::map<unsigned int, Unit*>* getUnitMap ();

        unsigned int getTurn () const;
        void setTurn (unsigned int turn);
};

#endif /* GAME_H */
