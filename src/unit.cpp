#include "unit.h"

Unit::Unit (unsigned int id) :
    HasProperties(),
    id (id)
{
}

Unit::~Unit ()
{
}

unsigned int Unit::getId () const
{
    return id;
}
