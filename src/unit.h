#ifndef UNIT_H
#define UNIT_H

#include "has_properties.h"

class Unit :
    public HasProperties
{
    protected:
        unsigned int id;

    public:
        Unit (unsigned int id);
        ~Unit ();

        unsigned int getId () const;
};

#endif /* UNIT_H */
