#ifndef GAME_RULES_H
#define GAME_RULES_H

class IGameRules
{
    public:
        IGameRules () {}
        virtual ~IGameRules () {}

        virtual bool gameEnded (const Game& g) = 0;
};

#endif /* GAME_RULES_H */
