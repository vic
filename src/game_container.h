#ifndef GAME_CONTAINER_H
#define GAME_CONTAINER_H

#include "game.h"
#include "player.h"
#include "game_rules.h"
#include <list>

/* This is a TURN-BASED game container */
class GameContainer
{
    protected:
        Game* game;
        std::list<Player*>* players;
        IGameRules* rules;
        bool started;
        bool ended;

    public:
        GameContainer (Game* game, IGameRules* rules);
        virtual ~GameContainer ();

        int addPlayer (Player* player);
        void start ();

        bool gameEnded () const;

    protected:
        int doPlayerTurn (Player* player);
        int onTurnBegin (Player* player);
        int onTurnEnd (Player* player);
};

#endif /* GAME_CONTAINER_H */
