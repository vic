CPPC=g++
CFLAGS=-Wall -pedantic -g -pthread
LIBS=
INCLUDE=src
SRCDIR=src

test: main.o tile.o hex_map_rules.o unit.o has_properties.o game_container.o game.o player.o board_generator.o
	$(CPPC) $(CFLAGS) $(LIBS) -I$(INCLUDE) $^ -o $@

%.o: $(SRCDIR)/%.cpp
	$(CPPC) $(CFLAGS) -I$(INCLUDE) $< -c -o $@

clean:
	rm -rf *.o
