#ifndef BOARD_GENERATOR_H
#define BOARD_GENERATOR_H

#include<map>
#include<string>
#include "board.h"
#include "tile.h"

class IBoardGenerator
{
    protected:
        static std::map<std::string, IBoardGenerator*> directory;

        std::string id;

    public:
        IBoardGenerator (const std::string& id);
        virtual ~IBoardGenerator ();

        virtual Board<Tile>* createBoard () = 0;

        const std::string getId () const;

        static int subscribe (IBoardGenerator* generator);
        static IBoardGenerator* get (const std::string& id);
};

#endif /* BOARD_GENERATOR_H */
