#ifndef PLAYER_H
#define PLAYER_H

#include "command.h"
#include "command_source.h"

class Player
{
    protected:
        unsigned int id;
        ICommandSource* cmdSrc;

    public:
        Player (unsigned int id, ICommandSource* cmdSrc);
        virtual ~Player ();

        unsigned int getId () const;

        ICommand* nextCommand ();
};

#endif /* PLAYER_H */
