#ifndef HAS_PROPERTIES_H
#define HAS_PROPERTIES_H

#include <map>
#include <string>

class HasProperties
{
    protected:
        std::map<std::string, std::string>* properties;

    public:
        HasProperties ();
        virtual ~HasProperties ();

        const std::string* getProperty (const std::string& key) const;
        bool setProperty (const std::string& key, const std::string& value);
        void eraseProperty (const std::string& key);
};

#endif /* HAS_PROPERTIES_H */
