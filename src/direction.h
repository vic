#ifndef DIRECTION_H
#define DIRECTION_H

#include <utility>

class IDirection
{
    public:
        IDirection () {};
        virtual ~IDirection () {};

        virtual bool checkBounds (unsigned int x, unsigned int y,
                unsigned int w, unsigned int h) const = 0;
        virtual std::pair<unsigned int, unsigned int> move (
                unsigned int x, unsigned int y) const = 0;
};

#endif /* DIRECTION_H */
