#include "tile.h"

Tile::Tile (unsigned int x, unsigned int y) :
    HasProperties (),
    x (x),
    y (y)
{
}

Tile::~Tile ()
{
}

unsigned int  Tile::getX () const
{
    return x;
}

unsigned int Tile::getY () const
{
    return y;
}
